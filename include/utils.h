#ifndef UTILS_H
#define UTILS_H

#define true 1
#define false 0

// Function Decls

/*
	strlen		- get length of string
	@param str	- string to measure
	returns length of string
*/
unsigned int strlen(char* str);

#endif /* UTILS_H */