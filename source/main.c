#include <utils.h>
#include <framebuffer.h>
#include <serial.h>

int kmain()
{
	serialinit();
	clear_fb();
	pos_reset();
	/* hacker voice - we're in */
	/* print some information */
	display_print_string("NLX\n", WHITE, BLACK);
	//cursor_new_row();
	display_print_string("Built on: ", WHITE, BLACK);
	display_print_string(__DATE__, WHITE, BLACK);
	display_print_string(" at ", WHITE, BLACK);
	display_print_string(__TIME__, WHITE, BLACK);
	display_print_string("\n", WHITE, BLACK);
	
	/* print it over serial as well */
	serialstr("NLX\n");
	serialstr("Built on: ");
	serialstr(__DATE__);
	serialstr(" at ");
	serialstr(__TIME__);
	serialstr("\n");
	return(0);
}
